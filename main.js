const container = document.querySelector('.container');
var inputValue = document.querySelector('.input');
const add = document.querySelector('.add');

if (window.localStorage.getItem("todos") == undefined) {
		var todos = [];
		window.localStorage.setItem("todos", JSON.stringify(todos));
}

var todosEX = window.localStorage.getItem("todos");
var todos = JSON.parse(todosEX);
console.log(todos);
add.addEventListener('click', newtask);

function newtask(){
	if (inputValue.value != "") {
		new item(inputValue.value);
		todos.push(inputValue.value);
		window.localStorage.setItem("todos", JSON.stringify(todos));
		inputValue.value = "";
	}
}

class item{
	constructor(name){
		this.createItem(name);
	}
	/*"name" ist der titel der einzelnen Todos */ 
	createItem(name){
		var itemBox = document.createElement('div');
		itemBox.classList.add('item');

		var title = document.createElement('input');
		title.type = "text";
		title.disabled = true;
		title.value = name;
		title.classList.add('item_title');

		var titleedit = document.createElement('button');
		titleedit.classList.add('edit');
		titleedit.innerHTML = "EDIT";
		titleedit.addEventListener('click', () => this.titleedit(title, name));

		var remove = document.createElement('button');
		remove.classList.add('remove');
		remove.innerHTML = "REMOVE";
		remove.addEventListener('click', () => this.remove(itemBox));

		var done = document.createElement('input');
		done.type = "checkbox";
		done.classList.add('done');
		done.addEventListener('click', () => this.done(itemBox, done));

		var date = document.createElement('span');
		date.classList.add('date');
		var d = new Date();
		let hours = d.getHours();
		let day = d.getDate();
		let mounth = d.getMonth() + 1;
		let year = d.getFullYear();
		date.innerHTML = hours + "h  " + day + "." + mounth + "." + year;
		console.log(date);

		var umbruch = document.createElement('br');

		var text = document.createElement('input');
		text.type = "text";
		text.disabled = true;
		text.value = "nähere details";
		text.classList.add('text');

		var textedit = document.createElement('button');
		textedit.classList.add('edit');
		textedit.innerHTML = "EDIT";
		textedit.addEventListener('click', () => this.textedit(text, name));

		container.appendChild(itemBox);
		itemBox.appendChild(done);
		itemBox.appendChild(title);
		itemBox.appendChild(titleedit);
		itemBox.appendChild(remove);
		itemBox.appendChild(date);
		itemBox.appendChild(umbruch);
		itemBox.appendChild(text);
		itemBox.appendChild(textedit);
	}

	titleedit(title, name){
		if(title.disabled == true){
			title.disabled = false;
		}
		else{
			title.disabled = true;
			var indexof = todos.indexOf(name);
			todos[indexof] = title.value;
			window.localStorage.setItem("todos", JSON.stringify(todos));
		}
	}

    remove(itemBox){
		itemBox.parentNode.removeChild(itemBox);
		var indexof = todos.indexOf(name);
		todos.splice(indexof, 1);
		window.localStorage.setItem("todos", JSON.stringify(todos));
	}
	
	done(itemBox, done) {
		if(itemBox.className == "itemdone"){
			itemBox.className = "item";
		}
		else{
			itemBox.className = "itemdone";
		}
	}
	
	textedit(text, name){
		if(text.disabled == true){
			text.disabled = false;
		 }
		else{
			text.disabled = true;
			var indexof = todos.indexOf(name);
			todos[indexof] = text.value;
			window.localStorage.setItem("todos", JSON.stringify(todos));
		}
	}
}

	for (var i = 0; i < todos.length; i++) {
		new item(todos[i]);
	}
